package assignmentQ2e;

public class Area extends Shape {
	@Override
	public void rectangleArea(float x, float y) {
		System.out.println("the area of the rectangle  " + x * y + " sq units");
		
	}
	@Override
	public void squareArea(float x) {
		System.out.println("the area of the square  " + Math.pow(x, 2) + " sq units");
	}
	@Override
	public void circleArea(double x) {
		double z = 3.14 * x * x;
		System.out.println("the area of the circle " + z + " sq units");
	}
	public static void main(String[] args) {
		Area ar = new Area();
		ar.squareArea(5);
		ar.rectangleArea(11, 12);
		ar.circleArea(2.5);
	}
}
