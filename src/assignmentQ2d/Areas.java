package assignmentQ2d;

public class Areas {
	void squareArea(float x) {
		System.out.println("the area of the square  " + Math.pow(x, 2) + " sq units");
	}
	void rectangleArea(float x, float y) {
		System.out.println("the area of the rectangle  " + x * y + " sq units");
	}
	void circleArea(double x) {
		double z = 3.14 * x * x;
		System.out.println("the area of the circle  " + z + " sq units");
	}

}
