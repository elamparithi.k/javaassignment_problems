package assignmentQ2a1;


	public class SmartTelephone extends Telephone {
		@Override
		public void disconnected() {
			System.out.println(" Telephone Class 1");
		}
		@Override
		public void lift() {
			System.out.println(" Telephone Class");
		}
		public static void main(String[] args) {
			SmartTelephone st = new SmartTelephone();
			st.disconnected();
			st.lift();
		}

}
